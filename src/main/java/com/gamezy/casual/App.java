package com.gamezy.casual;

import java.util.HashMap;
import java.util.Map;

import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.TopologyBuilder;

import com.gamezy.casual.config.Config;
import com.gamezy.casual.config.ConfigFactory;
import com.gamezy.casual.rules.RulesMeta;
import com.gamezy.casual.topology.builder.TopologyBuilderHelper;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args)
			throws AlreadyAliveException, InvalidTopologyException, AuthorizationException {
		new App().run(args);
	}

	private void run(String[] args) throws AlreadyAliveException, InvalidTopologyException, AuthorizationException {

		Config conf = ConfigFactory.getInstance().get(ConfigFactory.Envs.PROD);

		TopologyBuilder builder = TopologyBuilderHelper.getBuilder(conf, RulesMeta.getInstance());
		StormSubmitter.submitTopology("game-event-analyser", getTopologyConfs(conf), builder.createTopology());

	}

	private Map<String, Object> getTopologyConfs(Config conf) {
		Map<String, Object> topoConf = new HashMap<String, Object>();
		Map<String, String> confs = conf.get(ConfigFactory.Keys.TOPOLOGY);
		topoConf.put("topology.eventlogger.executors", Integer.valueOf(confs.get("topology.eventlogger.executors")));
		return topoConf;
	}
}
