package com.gamezy.casual.events;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GameUserEvent {

	private long gameId;
	private String game;
	private long userId;
	private int event;
	private String data;

}
