package com.gamezy.casual.config;

import java.util.Map;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Config {

	@NonNull
	Map<String, Map<String, String>> values;

	public Map<String, String> get(String grpName) {
		return values.get(grpName);
	}

}
