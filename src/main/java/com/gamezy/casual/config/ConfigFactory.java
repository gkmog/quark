package com.gamezy.casual.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConfigFactory {

	public static interface Keys {
		String KAFKA = "kafka";
		String TOPOLOGY = "topology";
	}

	public static interface Envs {
		String PROD = "prod";
	}

	private static final ConfigFactory _instance = new ConfigFactory();

	private static final Map<String, Config> configs = new HashMap<String, Config>();

	static {
		try {
			load();
		} catch (IOException e) {
			log.error("Error loading configs ", e);
			throw new RuntimeException(e);
		}

	}

	public Config get(String env) {
		return configs.get(env);
	}

	public static final ConfigFactory getInstance() {
		return _instance;
	}

	private static final void load() throws JsonParseException, JsonMappingException, IOException {
		loadFromFile(Envs.PROD);

	}

	private static void loadFromFile(String env) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("config/" + env + ".cfg");
		TypeReference<Map<String, Map<String, String>>> tRef = new TypeReference<Map<String, Map<String, String>>>() {
		};
		Map<String, Map<String, String>> values = mapper.readValue(is, tRef);
		Config cfg = new Config(values);
		configs.put(env, cfg);
	}

}
