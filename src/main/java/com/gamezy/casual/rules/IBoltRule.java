package com.gamezy.casual.rules;

public interface IBoltRule {

	String getTargetGame();

	int getTargetEvent();

	String getTargetBoltId();
	
	String getTargetStreamId();

}
