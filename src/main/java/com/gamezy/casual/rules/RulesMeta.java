package com.gamezy.casual.rules;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.storm.tuple.Fields;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamezy.casual.rules.dto.GameRulesDTO;
import com.gamezy.casual.rules.dto.WindowDTO;
import com.gamezy.casual.rules.dto.WindowEventRuleDTO;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RulesMeta {

	private static final RulesMeta _instance;

	public static final Fields GLOBAL_GROUPING_FILEDS = new Fields("matchId", "userId", "eventId");
	public static final Fields GLOBAL_ALL_FILEDS = new Fields("matchId", "userId", "eventId", "gameType");

	@Getter
	private Set<String> enabledGames = new HashSet<String>();
	@Getter
	private List<WindowDTO> enabledWindows = new ArrayList<WindowDTO>();

	@Getter
	private Map<String, Map<Integer, List<IBoltRule>>> gameEventRules = new HashMap<String, Map<Integer, List<IBoltRule>>>();
	static {
		try {
			_instance = init();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static final RulesMeta getInstance() {
		return _instance;
	}

	private static RulesMeta init() throws JsonParseException, JsonMappingException, IOException {
		RulesMeta meta = new RulesMeta();
		loadEnabledGames(meta);
		loadGameWiseRules(meta);
		return meta;
	}

	private static void loadGameWiseRules(RulesMeta meta) throws JsonParseException, JsonMappingException, IOException {
		for (String game : meta.enabledGames) {
			loadGameRule(game, meta);
		}

	}

	private static void loadGameRule(String game, RulesMeta meta)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("rules/" + game + ".rule");
		TypeReference<List<Map<String, String>>> tRef = new TypeReference<List<Map<String, String>>>() {
		};

		List<Map<String, String>> ruleValues = mapper.readValue(is, tRef);

		for (Map<String, String> ruleValue : ruleValues) {
			String ruleType = ruleValue.get("type");
			IBoltRule ruleDto = null;
			switch (ruleType) {
			case "window":
				ruleDto = createWindowRuleDTO(ruleValue, ruleType);
				break;
			default:
				throw new RuntimeException("Unknown rule type " + ruleType);
			}
			if (ruleDto != null) {
				Map<Integer, List<IBoltRule>> eventRules = meta.gameEventRules.get(game);
				if (eventRules == null) {
					eventRules = new HashMap<Integer, List<IBoltRule>>();
					eventRules.put(ruleDto.getTargetEvent(), new ArrayList<IBoltRule>());
					meta.gameEventRules.put(game, eventRules);
				}
				eventRules.get(ruleDto.getTargetEvent()).add(ruleDto);
			}
		}
	}

	private static IBoltRule createWindowRuleDTO(Map<String, String> ruleValues, String ruleType) {
		IBoltRule ruleDto;
		WindowEventRuleDTO dto = new WindowEventRuleDTO();
		dto.setType(ruleType);
		dto.setWindow(ruleValues.get("window"));
		dto.setEventId(Integer.valueOf(ruleValues.get("eventId")));
		dto.setMetric(ruleValues.get("metric"));
		dto.setThreshold(Integer.valueOf(ruleValues.get("threshold")));
		dto.setMessage(ruleValues.get("message"));
		ruleDto = dto;
		return ruleDto;
	}

	private static void loadEnabledGames(RulesMeta meta) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("rules/games.rule");
		GameRulesDTO dto = mapper.readValue(is, GameRulesDTO.class);
		meta.enabledGames = new HashSet<String>(Arrays.asList(dto.getGames()));
		meta.enabledWindows.addAll(Arrays.asList(dto.getWindows()));
	}

	public boolean isGameEnabled(String game) {
		return enabledGames.contains(game);
	}

	public List<IBoltRule> getBoltRules(String gameType, int event) {
		Map<Integer, List<IBoltRule>> eventRules = gameEventRules.get(gameType);
		if (eventRules == null)
			return null;
		return eventRules.get(event);
	}

}
