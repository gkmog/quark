package com.gamezy.casual.rules.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class GameRulesDTO {
	
	private String[] games;
	private WindowDTO[] windows;

}
