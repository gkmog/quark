package com.gamezy.casual.rules.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gamezy.casual.rules.IBoltRule;

import lombok.Data;

@Data
public class WindowEventRuleDTO implements IBoltRule {

	private String type;
	private String window;
	private String metric;
	private int eventId;
	private int threshold;
	private String message;
	@JsonIgnore
	private String gameType;

	@Override
	public String getTargetGame() {
		return gameType;
	}

	@Override
	public int getTargetEvent() {
		return eventId;
	}

	@Override
	public String getTargetBoltId() {
		return window;
	}

	@Override
	public String getTargetStreamId() {
		return window;
	}
}
