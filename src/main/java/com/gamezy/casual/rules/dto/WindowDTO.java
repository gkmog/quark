package com.gamezy.casual.rules.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class WindowDTO {
	
	private String name;
	private int size;
	private int freq;

}
