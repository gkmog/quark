package com.gamezy.casual.topology.bolt;

import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.windowing.TupleWindow;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SlidingWindowBolt extends BaseWindowedBolt {
	
	private OutputCollector collector;

	@Override
	public void execute(TupleWindow inputWindow) {
		// TODO Auto-generated method stub

	}

	@Override
	public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
		super.prepare(topoConf, context, collector);
		this.collector  = collector;
	}

}
