package com.gamezy.casual.topology.bolt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamezy.casual.rules.IBoltRule;
import com.gamezy.casual.rules.RulesMeta;
import com.gamezy.casual.rules.dto.WindowDTO;
import com.gamezy.casual.topology.bolt.dto.TempEventDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EventFilterAndRouterBolt implements IRichBolt {

	private OutputCollector collector;

	@Override
	public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void execute(Tuple input) {
		// log.debug("Received tuple %s", input.toString());
		System.out.println("Received tuple :" + input.toString());
		collector.ack(input);

		TempEventDTO dto = null;

		try {

			dto = convertToTempEventDTO(input);
		} catch (Exception e) {
			log.error("Failed to parse tuple %s, ERROR: %s", input, e);
			// bad format just ack
			collector.ack(input);
			return;
		}

		if (!isUserEvent(dto)) {
			collector.ack(input);
			return;
		}

		if (!isGameBeingAnalysed(dto)) {
			collector.ack(input);
			return;
		}

		List<IBoltRule> rulesForThisEvent = getTargetRuleBolts(dto);
		if (rulesForThisEvent == null || rulesForThisEvent.size() == 0) {
			collector.ack(input);
			return;
		}

		for (IBoltRule rule : rulesForThisEvent) {
			List<Object> tuple = convertToEventTuple(dto);
			collector.emit(rule.getTargetStreamId(), tuple);
			// anchore it here
		}

	}

	private boolean isGameBeingAnalysed(TempEventDTO dto) {
		return RulesMeta.getInstance().isGameEnabled(dto.getGameType());
	}

	private List<IBoltRule> getTargetRuleBolts(TempEventDTO dto) {
		return RulesMeta.getInstance().getBoltRules(dto.getGameType(), dto.getType());

	}

	private List<Object> convertToEventTuple(TempEventDTO dto) {

		// 1.matchId , 2. gameType 3. eventId, 4. userId
		return Arrays.asList(dto.getMatchId(), dto.getGameType(), dto.getType(), dto.getUserIds()[0]);
	}

	private boolean isUserEvent(TempEventDTO dto) {

		return dto.getUserIds() != null && dto.getUserIds().length > 0;
	}

	private TempEventDTO convertToTempEventDTO(Tuple tuple) throws JsonMappingException, JsonProcessingException {

		String value = tuple.getStringByField("value");
		ObjectMapper mapper = new ObjectMapper();
		TempEventDTO dto = mapper.readValue(value, TempEventDTO.class);
		long matchId = -1;
		String gameType = null;
		String[] segments = dto.getGameId().split("_");
		gameType = segments[0];
		matchId = Long.valueOf(segments[1]);
		dto.setMatchId(matchId);
		dto.setGameType(gameType);
		return dto;
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {

		Set<String> allStreams = new HashSet<String>();
		RulesMeta.getInstance().getEnabledWindows().forEach((win) -> {
			allStreams.add(win.getName());
		});
		System.out.println("declaring streams ");
		for (String stream : allStreams) {
			System.out.println("stream is " + stream);
			declarer.declareStream(stream, RulesMeta.GLOBAL_ALL_FILEDS);
		}
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

}
