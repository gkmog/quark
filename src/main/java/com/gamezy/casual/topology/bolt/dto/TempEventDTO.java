package com.gamezy.casual.topology.bolt.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class TempEventDTO {

	private String gameId;
	private int type;
	private String data;
	private long[] userIds;
	@JsonIgnore
	private String gameType;
	@JsonIgnore
	private long matchId ;

}
