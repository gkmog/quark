package com.gamezy.casual.topology.builder;

import java.util.Map;

import org.apache.storm.kafka.spout.FirstPollOffsetStrategy;
import org.apache.storm.kafka.spout.KafkaSpout;
import org.apache.storm.kafka.spout.KafkaSpoutConfig;

import com.gamezy.casual.config.Config;
import com.gamezy.casual.config.ConfigFactory;

public class KafkaSpoutBulder {

	public static KafkaSpout<String, String> build(Config config) {
		Map<String, String> kafkaConfiggs = config.get(ConfigFactory.Keys.KAFKA);
		KafkaSpoutConfig<String, String> cfg = KafkaSpoutConfig
				.builder(kafkaConfiggs.get("bootstrap.servers"), kafkaConfiggs.get("event.topic.name"))
				.setFirstPollOffsetStrategy(FirstPollOffsetStrategy.valueOf(kafkaConfiggs.get("first.poll.strategy")))
				.build();
		return new KafkaSpout<String, String>(cfg);

	}

}
