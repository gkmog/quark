package com.gamezy.casual.topology.builder;

import org.apache.storm.kafka.spout.KafkaSpout;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseWindowedBolt.Duration;

import com.gamezy.casual.config.Config;
import com.gamezy.casual.rules.RulesMeta;
import com.gamezy.casual.rules.dto.WindowDTO;
import com.gamezy.casual.topology.bolt.EventFilterAndRouterBolt;
import com.gamezy.casual.topology.bolt.SlidingWindowBolt;

public class TopologyBuilderHelper {

	private static final String SOURCE_KAFKA_SPOUT_ID = "source-kafka";
	private static final String FILTER_ROUTER_BOLT_ID = "filter-router-bolt";

	public static TopologyBuilder getBuilder(Config config, RulesMeta meta) {
		TopologyBuilder builder = new TopologyBuilder();
		// get the source kafka spout
		KafkaSpout<String, String> sourceKafkaSpout = KafkaSpoutBulder.build(config);
		builder.setSpout(SOURCE_KAFKA_SPOUT_ID, sourceKafkaSpout);
		builder.setBolt(FILTER_ROUTER_BOLT_ID, new EventFilterAndRouterBolt()).shuffleGrouping(SOURCE_KAFKA_SPOUT_ID);
		// add one bolt for each type of window, each window bolt takes a unique stream
		for (WindowDTO window : meta.getEnabledWindows()) {
			builder.setBolt(window.getName(),
					new SlidingWindowBolt().withWindow(Duration.seconds(window.getSize()),
							Duration.seconds(window.getFreq())))
					.fieldsGrouping(FILTER_ROUTER_BOLT_ID, window.getName(), RulesMeta.GLOBAL_GROUPING_FILEDS);
		}
		return builder;
	}

}
